#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 14:23:43 2016

@author: ayumi
"""

import cv2
import numpy as np
import time
import datetime
import matplotlib.pyplot as plt

def flame_sub(im1,im2,im3,th,blur):

    d1 = cv2.absdiff(im3, im2)
    d2 = cv2.absdiff(im2, im1)
    diff = cv2.bitwise_and(d1, d2)
    # 差分が閾値より小さければTrue
    mask = diff < th
    # 背景画像と同じサイズの配列生成
    im_mask = np.empty((im1.shape[0],im1.shape[1]),np.uint8)
    im_mask[:][:]=255
    # Trueの部分（背景）は黒塗り
    im_mask[mask]=0
    # ゴマ塩ノイズ除去
    im_mask = cv2.medianBlur(im_mask,blur)

    return  im_mask


if __name__ == '__main__':

    cam = cv2.VideoCapture(0)
    cam.set(3, 800)  # Width
    cam.set(4, 600)  # Heigh
    cam.set(5, 15)   # FPS
    im1 = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
    im2 = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
    im3 = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
    
    num = 0
    Zero_list = []

    while num < 300:
        # フレーム間差分計算
        im_fs = flame_sub(im1,im2,im3,5,7)
        
        cv2.imshow("Input",im2)
        cv2.imshow("Motion Mask",im_fs)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
        im1 = im2
        im2 = im3
        im3 = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
        
        fileName = "pic" + datetime.datetime.today().strftime('%Y%m%d_%H%M%S') + ".jpg"
        
        #cv2.imwrite(fileName,im3)
        #cv2.imwrite("frame_sub.jpg",im_fs)
        
        NWhitePix = cv2.countNonZero(im_fs)
        print('{0} countNonZero : {1:d}'.format(num, NWhitePix))
        
        Zero_list.append(NWhitePix)
        
        
        num +=1
        time.sleep(1)

cv2.destroyAllWindows()        
x = Zero_list
plt.hist(x, bins=30)
plt.show()
