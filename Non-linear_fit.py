import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import curve_fit

filename = ["new_text.txt","new_text2.txt","new_text3.txt","new_text4.txt","new_text5.txt","new_text6.txt"]

for i in range (len(filename)):

    y_data = np.array([])
    x_data = np.array([])

    for line in open(filename[i],"r"):
        tmp_data = line.split()
        if len(tmp_data) == 0:
            continue
        x_data = np.append(x_data,np.deg2rad(float(tmp_data[0])))
        y_data = np.append(y_data,float(tmp_data[1]))

        def cos2_func(x,a,b,c,d):
            return a*(np.cos(b*x+c))**2+d
        
        parameter_initial = np.array([200,1,10,0])
        popt,pcov = curve_fit(cos2_func,x_data,y_data,p0=parameter_initial)
        eig, P = np.linalg.eig(pcov)
        D = np.dot(np.dot(np.linalg.inv(P),pcov),P)

        xd = np.arange(np.deg2rad(-100),np.deg2rad(100),0,1)
        estimated_curve = cos2_func(xd,popt[0],popt[1],popt[2],popt[3])
        plt.figure(figsize=(12,9))
        plt.rcParams["font.size"] = 25
        plt.gca().yaxis.set_major_formatter(plt.FormatStrFormatter("%.1f"))
        plt.scatter(x_data,y_data)

        plt.xlabel("Angle $¥it{0}$[rad]")
        plt.xlabel("Intensity $¥it{I}$[µA]")
        tempstr = "{:.4g}".format(popt[3])
        print(tempstr)
        if not "." in tempstr:
            tempstr += ".0"

        plt.plot(xd,estimated_curve,c="r",label="$¥it{a}$ = "+'{:.4g}'.format(popt[0])+" ± "+'{:.1f}'.format(np.sqrt(D[0][0]))+"¥n$¥it{b}$ = "+'{:.4g}'.format(popt[1])+"±"+'{:.3f}'.format(np.sqrt(D[1][1]))+"¥n$¥it{c}$="+'{:.4g}'.format(popt[2])+"±"+'{:.2f}'.format(np.sqrt(D[2][2]))+"¥n$¥it{d}$="+tempstr+"±"+'{:.1f}'.format(np.sqrt(D[3][3])) )
        plt.legend()
        plt.savefig("test"+str(filenames[i][-7:-4]))
        
    