import numpy as np
import matplotlib.pyplot as plt
import time
times = []
number = []
for i in range(2,10000000):
    start_time = time.time()
    for h in range(2,int(i**(1/2))+1):
        if i%h == 0 :
            break
    else :
        elapsed_time = time.time()-start_time
        times.append(elapsed_time)
        number.append(i)       
plt.plot(number,times)
plt.show()