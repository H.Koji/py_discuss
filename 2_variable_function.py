import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

x = y = np.linspace(-2,2)
x, y = np.meshgrid(x, y)

g1 = lambda x: x[0]**2-3*x[1]
ax = Axes3D(plt.figure())
ax.plot_wireframe(x,y,g1([x,y]),rstride=2,cstride=2)
plt.show()